@extends('layouts.app')

@section('content')
<h2 class="mt-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">Crawl of {{ $crawl->site_name }} started {{ $crawl->started_at }}</h2>
<div class="mb-6 mt-2 text-sm uppercase">{{ $urls->count() }} urls</div>

<div class="mb-6">
    {{-- <form action="{{ route('crawls.update', $crawl) }}" method="POST" class="inline-block">
        @method('PUT')
        @csrf
        <button class="text-teal-600 rounded border border-teal-600 px-3 py-2 uppercase text-xs font-bold">Restart</button>
    </form> --}}
    <form action="{{ route('crawls.destroy', $crawl) }}" method="POST" class="inline-block">
        @method('DELETE')
        @csrf
        <button class="text-red-600 rounded border border-red-600 px-3 py-2 uppercase text-xs font-bold">Delete</button>
    </form>
</div>

<table class="w-full whitespace-no-wrap">
    <thead>
        <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
            <th class="px-4 py-3">URL</th>
            <th class="px-4 py-3">Result</th>
            <th class="px-4 py-3">Last Crawled</th>
            <th class="px-4 py-3">Details</th>
        </tr>
    </thead>
    <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
        @foreach ($urls as $url)
        <tr class="text-gray-700 dark:text-gray-400">
            <td class="px-4 py-3 text-xs">
                @php
                    $urlParts = parse_url($url->url);
                    $urlString = (isset($urlParts['path']) ? $urlParts['path'] : '').(isset($urlParts['query']) ? '?'.$urlParts['query'] : '').(isset($urlParts['fragment']) ? '#'.$urlParts['fragment'] : '');
                @endphp
                {{ $urlString }}
            </td>
            <td class="px-4 py-3 text-sm">
                <span class="rounded bg-gray-300 text-white px-1 py-1 text-xs font-bold mr-1">{{ $url->attempts }}</span>
                {{ $url->result }}
            </td>
            <td class="px-4 py-3 text-sm">{{ $url->last_crawled_at }}</td>
            <td class="px-4 py-3 text-sm"><pre>{{ $url->details }}</pre></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection