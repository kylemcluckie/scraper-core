@extends('layouts.app')

@section('content')
<h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">Crawls</h2>

<table class="w-full whitespace-no-wrap">
    <thead>
        <tr class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800">
            <th class="px-4 py-3">Site</th>
            <th class="px-4 py-3">Status</th>
            <th class="px-4 py-3">Started</th>
            <th class="px-4 py-3">Ended</th>
            <th class="px-4 py-3">Duration</th>
            <th class="px-4 py-3"></th>
        </tr>
    </thead>
    <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
        @foreach ($crawls as $crawl)
        <tr class="text-gray-700 dark:text-gray-400">
            <td class="px-4 py-3 text-sm">
                {{ $crawl->store_id }}
            </td>
            <td class="px-4 py-3 text-sm">
                @php
                    $running = '';
                    $processResult = posix_getpgid($crawl->pid);
                    if ($processResult != false) {
                        $cmd = trim(file_get_contents("/proc/$crawl->pid/cmdline"), "\0");
                        if (\Illuminate\Support\Str::startsWith($cmd, "php\x00artisan\x00crawler")) {
                            $running = 'Running';
                        }
                    }
                @endphp
                {{ $running }}
            </td>
            <td class="px-4 py-3 text-sm">
                {{ $crawl->started_at }}
            </td>
            <td class="px-4 py-3 text-sm">
                {{ $crawl->ended_at }}
            </td>
            <td class="px-4 py-3 text-sm">
                @if ($crawl->ended_at != null)
                    {{ $crawl->ended_at->diffInMinutes($crawl->started_at) }} min
                @endif
            </td>
            <td class="px-4 py-3 text-sm">
                <a href="{{ route('crawls.show', $crawl) }}">View</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
