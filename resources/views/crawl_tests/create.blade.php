@extends('layouts.app')

@section('content')
<h2 class="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">Crawl Test</h2>

<form action="{{ route('crawl-tests.store') }}" method="POST">
    @csrf
    <select name="store">
        @foreach (config('crawling.stores') as $store)
            <option value="{{ $store->getStoreId() }}">{{ $store->getName() }}</option>
        @endforeach
    </select>
    <input type="text" name="url">
    <button type="submit">Crawl</button>
</form>

@endsection
