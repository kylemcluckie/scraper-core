## Installation

This package can be installed via Composer:

``` bash
composer require fallfoundry/scraper-core
php artisan migrate
```