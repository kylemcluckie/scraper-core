<?php

namespace FallFoundry\Scraper\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Crawl extends Model
{
    use HasFactory;
    
    protected $fillable = ['store_id', 'started_at', 'ended_at', 'pid'];

    protected $dates = [
        'started_at',
        'ended_at',
    ];

    public function getStore()
    {
        return config('crawling.stores')[$this->store_id];
    }

    public function urls()
    {
        return $this->hasMany('FallFoundry\Scraper\Models\CrawlUrl');
    }

    public function isComplete()
    {
        return $this->ended_at != null;
    }
}
