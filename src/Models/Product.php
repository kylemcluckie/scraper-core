<?php

namespace FallFoundry\Scraper\Models;

use Carbon\Carbon;
use Money\Currency;
use Cknow\Money\Money;
use Laravel\Scout\Searchable;
use Spatie\Sluggable\HasSlug;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, Searchable, HasSlug, SoftDeletes;
    
    protected $fillable = ['name', 'sku', 'url', 'description',
        'stock', 'condition', 'store_id'];

    protected $casts = [
        'price_history' => 'array',
    ];

    public function getPrice()
    {
        return new Money($this->price, new Currency($this->currency));
    }

    public function setPriceByDecimal($decimalPrice, $currencyCode)
    {
        $this->setPriceAndCurrency(intval(floatval($decimalPrice) * 100), $currencyCode);
    }

    public function setPriceByMoney($money)
    {
        $this->setPriceAndCurrency($money->getAmount(), $money->getCurrency()->getCode());
    }

    private function setPriceAndCurrency($priceCents, $currencyCode)
    {
        if ($priceCents != $this->price || $currencyCode != $this->currency)
        {
            $this->addPriceHistory($priceCents, $currencyCode);
        }

        $this->price = $priceCents;
        $this->currency = $currencyCode;
    }

    private function addPriceHistory($priceCents, $currencyCode)
    {
        $ph = $this->price_history;
        if ($ph === null) $ph = [];
        $entry = [ 'price' => $priceCents, 'currency' => $currencyCode ];
        $ph[Carbon::now()->toString()] = $entry;
        $this->price_history = $ph; // This is quite ugly, but for some reason I can't just do all of this directly on $this->price_history
    }

    public function getStore()
    {
        return config('crawling.stores')[$this->store_id];
    }
    
    public function toSearchableArray()
    {
        $searchableFields = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description
        ];

        return $searchableFields;
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['id', 'name'])
            ->saveSlugsTo('slug')
            ->slugsShouldBeNoLongerThan(50);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
