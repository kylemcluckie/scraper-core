<?php

namespace FallFoundry\Scraper\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CrawlUrl extends Model
{
    use HasFactory;
    
    protected $fillable = ['url', 'crawl_id', 'result', 'details'];

    protected $dates = [
        'last_crawled_at'
    ];

    public function registerCrawl()
    {
        $this->attempts = $this->attempts + 1;
        $this->last_crawled_at = Carbon::now();
    }
}
