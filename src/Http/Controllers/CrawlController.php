<?php

namespace FallFoundry\Scraper\Http\Controllers;

use FallFoundry\Scraper\Models\Crawl;

class CrawlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crawls = Crawl::all();
        return view('scraper::crawls.index', compact('crawls'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Crawl  $crawl
     * @return \Illuminate\Http\Response
     */
    public function show(Crawl $crawl)
    {
        $urls = $crawl->urls()->orderBy('attempts')->get();
        return view('scraper::crawls.show', compact('crawl', 'urls'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Crawl  $crawl
     * @return \Illuminate\Http\Response
     */
    public function destroy(Crawl $crawl)
    {
        $crawl->urls()->delete();
        $crawl->delete();

        return redirect(route('crawls.index'));
    }
}
