<?php

namespace FallFoundry\Scraper\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CrawlTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('scraper::crawl_tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = $request->get('store');
        $url = new \GuzzleHttp\Psr7\Uri($request->get('url'));
        $site = config('crawling.stores')[$store];

        $shouldCrawl = $site->shouldCrawl($url);

        if ($shouldCrawl)
        {
            $client = new Client();
            $response = $client->request('GET', $url);
    
            dd($site->extract($response->getBody(), $url));
        }
        else
        {
            return 'Should not crawl';
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
