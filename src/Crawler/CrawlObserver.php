<?php

namespace FallFoundry\Scraper\Crawler;

use Carbon\Carbon;
use Psr\Http\Message\UriInterface;
use FallFoundry\Scraper\Crawler\ProductDeleteProcessor;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

class CrawlObserver extends \Spatie\Crawler\CrawlObservers\CrawlObserver
{
    private $site, $crawl, $productDeleter;

    public function __construct($site, $crawl, ProductDeleteProcessor $productDeleter)
    {
        $this->site = $site;
        $this->crawl = $crawl;
        $this->productDeleter = $productDeleter;
    }

    public function willCrawl(UriInterface $url)
    {
    }
    
    public function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null)
    {
        $crawlUrl = $this->crawl->urls()->firstWhere('url', $url);

        $html = $response->getBody();
        try {
            $this->site->extract((string)$html, $url);
            $crawlUrl->result = 'success';
            $crawlUrl->last_crawled_at = Carbon::now();
            $crawlUrl->save();
        } catch(\Error | \RuntimeException | \Throwable $e) {
            $crawlUrl->result = 'extract failed';
            $crawlUrl->details = $e->getTraceAsString();
            $crawlUrl->last_crawled_at = Carbon::now();
            $crawlUrl->save();
        }
    }

    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null)
    {
        $request = $requestException->getRequest();
        $response = $requestException->getResponse();
        
        $crawlUrl = $this->crawl->urls()->firstWhere('url', $url);
        $crawlUrl->result = 'request failed';
        $crawlUrl->last_crawled_at = Carbon::now();
        $crawlUrl->save();
    }

    public function finishedCrawling()
    {
        $this->crawl->ended_at = Carbon::now();
        $this->crawl->save();

        $this->productDeleter->deleteProductsAfterCrawl($this->crawl);
    }
}
