<?php

namespace FallFoundry\Scraper\Crawler;

use Carbon\Carbon;
use FallFoundry\Scraper\Models\Crawl;
use Spatie\Crawler\Crawler;
use FallFoundry\Scraper\Crawler\DBCrawlQueue;
use FallFoundry\Scraper\Crawler\CrawlRequestFulfilled;

class Scraper
{
    public function crawlSite($site, $crawl = null)
    {
        if (!$crawl)
        {
            $crawl = new Crawl(['store_id' => $site->getStoreId(), 'started_at' => Carbon::now()]);
        }

        $crawl->pid = getmypid();
        $crawl->save();

        $observer = new CrawlObserver($site, $crawl, new \FallFoundry\Scraper\Crawler\ProductDeleteProcessor);
        $queue = new DBCrawlQueue($crawl);
        $fulfiller = CrawlRequestFulfilled::class;
        
        Crawler::create()
            ->setCrawlObserver($observer)
            ->setCrawlProfile($site)
            ->setCrawlQueue($queue)
            ->setBrowsershot($site->getBrowsershot())
            ->setCrawlFulfilledHandlerClass($fulfiller)
            ->setDelayBetweenRequests($site->getRequestInterval())
            ->startCrawling($site->getUrl());
    }
}
