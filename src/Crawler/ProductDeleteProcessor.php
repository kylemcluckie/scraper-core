<?php

namespace FallFoundry\Scraper\Crawler;

use FallFoundry\Scraper\Models\Product;

class ProductDeleteProcessor
{
    public static function deleteProductsAfterCrawl($crawl)
    {
        $products = Product::where('store_id', $crawl->store_id)->get();
        foreach($products as $p)
        {
            $crawlUrlCount = $crawl->urls()->where('url', $p->url)->count();
            if ($crawlUrlCount == 0)
            {
                $p->clearMediaCollection();
                $p->delete();
            }
        }
    }
}
