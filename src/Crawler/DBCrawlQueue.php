<?php

namespace FallFoundry\Scraper\Crawler;

use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlUrl;
use Spatie\Crawler\Exceptions\InvalidUrl;
use Spatie\Crawler\Exceptions\UrlNotFoundByIndex;
use FallFoundry\Scraper\Models\CrawlUrl as CrawlUrlModel;
use GuzzleHttp\Psr7\Uri;

class DBCrawlQueue implements \Spatie\Crawler\CrawlQueues\CrawlQueue
{
    private $crawl;

    public function __construct($crawl)
    {
        $this->crawl = $crawl;
    }

    public function add(CrawlUrl $crawlUrl): DBCrawlQueue
    {
        $urlString = (string) $crawlUrl->url;

        if (! $this->has($crawlUrl)) {
            $url = new CrawlUrlModel(['url' => $urlString, 'crawl_id' => $this->crawl->id]);
            $url->save();
        }

        return $this;
    }

    public function hasPendingUrls(): bool
    {
        return (bool) $this->getFirstPendingUrl();
    }

    public function getUrlById($id): CrawlUrl
    {
        $url = $this->crawl->urls()->firstWhere('url', $id);
        if (!$url) throw new UrlNotFoundByIndex("Crawl url {$id} not found in collection.");
        return CrawlUrl::create(new Uri($url->url), null, $id);
    }

    public function hasAlreadyBeenProcessed(CrawlUrl $crawlUrl): bool
    {
        $urlString = (string) $crawlUrl->url;

        $url = $this->crawl->urls()->firstWhere('url', $urlString);

        if ($url->result == 'success') {
            return true;
        }

        return false;
    }

    public function markAsProcessed(CrawlUrl $crawlUrl): void
    {
        $urlString = (string) $crawlUrl->url;

        $url = $this->crawl->urls()->firstWhere('url', $urlString);
        $url->registerCrawl();
        if ($url->result == '') $url->result = 'processed';
        $url->save();
    }

    /**
     * @param CrawlUrl|UriInterface $crawlUrl
     *
     * @return bool
     */
    public function has($crawlUrl): bool
    {
        if ($crawlUrl instanceof CrawlUrl) {
            $urlString = (string) $crawlUrl->url;
        } elseif ($crawlUrl instanceof UriInterface) {
            $urlString = (string) $crawlUrl;
        } else {
            throw InvalidUrl::unexpectedType($crawlUrl);
        }

        $url = $this->crawl->urls()->firstWhere('url', $urlString);

        return (bool) $url;
    }

    public function getFirstPendingUrl(): ?CrawlUrl
    {
        $pending = $this->crawl->urls()
            ->where('result', '<>', 'success')
            ->where('attempts', '<', 2)
            ->orderBy('attempts', 'ASC')
            ->first();

        return $pending ? CrawlUrl::create(new Uri($pending->url), null, $pending->url) : null;
    }
}
