<?php

namespace FallFoundry\Scraper\Crawler;

use Spatie\Crawler\CrawlerRobots;
use Psr\Http\Message\ResponseInterface;
use function GuzzleHttp\Psr7\stream_for;
use Spatie\Crawler\ResponseWithCachedBody;
use Spatie\Crawler\CrawlProfiles\CrawlSubdomains;

class CrawlRequestFulfilled extends \Spatie\Crawler\Handlers\CrawlRequestFulfilled
{
    public function __invoke(ResponseInterface $response, $index)
    {
        $body = $this->getBody($response);

        $robots = new CrawlerRobots(
            $response->getHeaders(),
            $body,
            $this->crawler->mustRespectRobots()
        );

        $crawlUrl = $this->crawler->getCrawlQueue()->getUrlById($index);

        if ($this->crawler->getCrawlProfile()->useJSForPage($crawlUrl, $response)) {
            $body = $this->getBodyAfterExecutingJavaScript($crawlUrl->url);

            $response = $response->withBody(stream_for($body));
        }

        $responseWithCachedBody = ResponseWithCachedBody::fromGuzzlePsr7Response($response);
        $responseWithCachedBody->setCachedBody($body);

        if ($robots->mayIndex()) {
            $this->handleCrawled($responseWithCachedBody, $crawlUrl);
        }

        if (! $this->crawler->getCrawlProfile() instanceof CrawlSubdomains) {
            if ($crawlUrl->url->getHost() !== $this->crawler->getBaseUrl()->getHost()) {
                return;
            }
        }

        if (! $robots->mayFollow()) {
            return;
        }

        $baseUrl = $this->getBaseUrl($response, $crawlUrl);

        $this->linkAdder->addFromHtml($body, $baseUrl);

        usleep($this->crawler->getDelayBetweenRequests());
    }
}
