<?php

namespace FallFoundry\Scraper\Crawler;

use Carbon\Carbon;
use FallFoundry\Scraper\Models\Product;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Support\Str;
use Psr\Http\Message\UriInterface;
use Spatie\Browsershot\Browsershot;
use Spatie\Crawler\CrawlProfiles\CrawlProfile;

class Site extends CrawlProfile
{
    protected $storeId = '';
    protected $name = '';
    protected $baseUrl = '';
    protected $minHoursBetweenCrawl = 16;
    protected $requestInterval = 150;

    public function __construct()
    {
        if (!$this->baseUrl instanceof UriInterface) {
            $this->baseUrl = new Uri($this->baseUrl);
        }
    }

    public function getStoreId()
    {
        return $this->storeId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getUrl()
    {
        return $this->baseUrl;
    }

    public function getRequestInterval()
    {
        return $this->requestInterval;
    }

    public function shouldCrawl(UriInterface $url): bool
    {
        // Only crawl the current domain
        if ($this->baseUrl->getHost() !== $url->getHost()) return false;

        // If product update in last X hours, don't bother crawling
        $product = Product::firstWhere('url', $url);
        if ($product && $product->updated_at > Carbon::now()->subHours($this->minHoursBetweenCrawl)) return false;

        // Don't crawl images, pdfs, etc
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        if (in_array(strtolower($ext), ['png', 'jpeg', 'jpg', 'gif', 'pdf'])) return false;

        return true;
    }

    protected function urlPathStartsWithAny($url, $words)
    {
        $path = $url->getPath();

        foreach ($words as $word)
        {
            if (Str::startsWith($path, $word)) return true;
        }
    }

    protected function urlContainsQueryKeysAny($url, $keys)
    {
        $queryStr = parse_url($url, PHP_URL_QUERY);
        parse_str($queryStr, $params);

        foreach ($keys as $key)
        {
            if (array_key_exists($key, $params)) return true;
        }
    }

    protected function urlPathContainsAny($url, $words)
    {
        $path = $url->getPath();

        foreach ($words as $word)
        {
            if (Str::contains($path, $word)) return true;
        }
    }

    public function useJSForPage($url, $response)
    {
        return false;
    }

    public function getBrowsershot()
    {
        $browsershot = new Browsershot();
        return $browsershot;
    }

    protected function buildProduct($url, $details)
    {
        $product = Product::withTrashed()->firstWhere('url', $url);
        if (!$product) $product = new Product();

        // Restore the product if previously deleted
        if ($product->trashed()) $product->restore();
        // Fill it with the data
        $product->fill($details);

        return $product;
    }

    protected function attachImagesToProduct($product, $imageUrls)
    {
        // Check if all the images that we have already are the same
        $imagesTheSame = true;
        foreach($imageUrls as $imageUrl)
        {
            $alreadyHasImage = false;
            $imageName = basename(parse_url($imageUrl, PHP_URL_PATH));
            foreach ($product->getMedia() as $image)
            {
                if ($image->file_name == $imageName)
                {
                    $alreadyHasImage = true;
                    break;
                }
            }

            if (!$alreadyHasImage)
            {
                $imagesTheSame = false;
                break;
            }
        }

        // If they are not the same we are going to redo all of them (simple, and should happen rarely)
        if (!$imagesTheSame)
        {
            $product->clearMediaCollection();

            if ($imageUrls[0])
            {
                if (Str::startsWith($imageUrls[0], '//')) $imageUrls[0] = 'https:' . $imageUrls[0];
                $product->addMediaFromUrl($imageUrls[0])->toMediaCollection();
            }

            // NOTE: used to get every image, but not necessary currently, so why bother?
            // foreach($imageUrls as $imageUrl)
            // {
            //     if (Str::startsWith($imageUrl, '//')) $imageUrl = 'https:' . $imageUrl;
            //     $product->addMediaFromUrl($imageUrl)->toMediaCollection();
            // }
        }

        return $product;
    }

    public function extract($html, $url)
    {
    }
}