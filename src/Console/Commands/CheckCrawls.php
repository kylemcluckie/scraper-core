<?php

namespace FallFoundry\Scraper\Console\Commands;

use Illuminate\Console\Command;
use FallFoundry\Scraper\Models\Crawl;
use FallFoundry\Scraper\Crawler\Scraper;

class CheckCrawls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and restart crawlers.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Scraper $scraper)
    {
        foreach(config('crawling.stores') as $store)
        {
            // Get last crawl for this store
            $crawl = Crawl::where('store_id', $store->getStoreId())->orderBy('started_at', 'DESC')->first();
            // If there is one that is not complete, start it
            if ($crawl && !$crawl->isComplete())
            {
                // If no process was found on pid
                if (posix_getpgid($crawl->pid) === false)
                {
                    $scraper->crawlSite($store, $crawl);
                    return;
                }

                // If the process found was not named correctly
                $filename = "/proc/$crawl->pid/cmdline";
                if (file_exists($filename))
                {
                    $cmd = trim(file_get_contents("/proc/$crawl->pid/cmdline"), "\0");
                    if (!\Illuminate\Support\Str::startsWith($cmd, "php\x00artisan\x00crawler"))
                    {
                        $scraper->crawlSite($store, $crawl);
                        return;
                    }
                }
            }
        }
    }
}