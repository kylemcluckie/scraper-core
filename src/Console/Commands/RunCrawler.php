<?php

namespace FallFoundry\Scraper\Console\Commands;

use Illuminate\Console\Command;
use FallFoundry\Scraper\Crawler\Scraper;

class RunCrawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:run {storeId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the crawler.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Scraper $scraper)
    {
        $site = config('crawling.stores')[$this->argument('storeId')];
        if (!$site) return;
        
        $scraper->crawlSite($site);
    }
}
