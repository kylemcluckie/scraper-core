<?php

namespace FallFoundry\Scraper;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ScraperServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->registerRoutes();
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'scraper');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->commands([
                \FallFoundry\Scraper\Console\Commands\RunCrawler::class,
                \FallFoundry\Scraper\Console\Commands\CheckCrawls::class,
            ]);
        }
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });
    }
    
    protected function routeConfiguration()
    {
        return [
            'middleware' => ['web'],
        ];
    }
}
