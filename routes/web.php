<?php

use Illuminate\Support\Facades\Route;
use FallFoundry\Scraper\Http\Controllers\CrawlController;
use FallFoundry\Scraper\Http\Controllers\CrawlTestController;

Route::group(['middleware' => 'auth'], function () {
    Route::resource('crawls', CrawlController::class);
    Route::resource('crawl-tests', CrawlTestController::class);
});